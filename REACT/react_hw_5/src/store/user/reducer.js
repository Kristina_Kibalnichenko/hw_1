import { SET_CART_VALUES, DEL_CART_FROM_LOCALSTORAGE } from "./types"

const initialStore = {
    name: '',
    surname: '',
    age: null,
    phone: '',
    delAddress: '',
}

const reducer = (state = initialStore, action) => {
  switch(action.type) {
    case SET_CART_VALUES:
      const {name, surname, age, phone, delAddress} = action.payload
      return {...state, name: name, surname: surname, age: age, phone: phone, delAddress: delAddress}
    case DEL_CART_FROM_LOCALSTORAGE:
      localStorage.removeItem('cart')
      return {...state}
    default:
      return state
  }
}

export default reducer;