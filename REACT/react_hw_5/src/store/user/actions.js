import { SET_CART_VALUES, DEL_CART_FROM_LOCALSTORAGE } from "./types"

export const setCartValues = (values) => ({type: SET_CART_VALUES, payload: values})
export const deleteLocalStorageCart = () => ({type: DEL_CART_FROM_LOCALSTORAGE})