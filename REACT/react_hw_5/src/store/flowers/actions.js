import { LOAD_FLOWERS_REQUEST, LOAD_FLOWERS_SUCCESS, LOAD_FLOWERS_FAILURE, CHANGE_FLOWERS, CHANGE_LOADING_STATE } from "./types"

export const loadFlowersRequest = () => ({type: LOAD_FLOWERS_REQUEST})
export const loadFlowersSuccess = (data) => ({type: LOAD_FLOWERS_SUCCESS, payload: data})
export const loadFlowersFailure = (data) => ({type: LOAD_FLOWERS_FAILURE, payload: data})
export const changeFlowersList = (data) => ({type: CHANGE_FLOWERS, payload: data})
export const changeLoadingState = () => ({type: CHANGE_LOADING_STATE, payload: false})