import axios from "axios"
import { loadFlowersRequest, loadFlowersSuccess, loadFlowersFailure } from "./actions"

const normalizeData = (data) => {
    const favor = JSON.parse(localStorage.getItem('favor')) || []
    const cart = JSON.parse(localStorage.getItem('cart')) || []
    return data.map(elem => {
        elem.isFavorite = favor.includes(elem.vendorCode)
        let flag = true
        cart.forEach(item => {
        if (item.vendorCode === elem.vendorCode) {
            elem.amountAtCart = item.amountAtCart
            flag = false
        } 
        })
        if (flag) {
        elem.amountAtCart = 0
        }
        return elem
    })
}

export const getItems = () => (dispatch) => {
    dispatch(loadFlowersRequest())
    axios('/itemslist.json')
      .then(result => {
        dispatch(loadFlowersSuccess(normalizeData(result.data.items)))
      })
      .catch((error) => {
        dispatch(loadFlowersFailure(error.message))
      })
}