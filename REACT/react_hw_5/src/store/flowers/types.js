export const CHANGE_LOADING_STATE = "CHANGE_LOADING_STATE"
export const CHANGE_FLOWERS = "CHANGE_FLOWERS"
export const LOAD_FLOWERS_REQUEST = "LOAD_FLOWERS_REQUEST"
export const LOAD_FLOWERS_SUCCESS = "LOAD_FLOWERS_SUCCESS"
export const LOAD_FLOWERS_FAILURE = "LOAD_FLOWERS_FAILURE"