import { CHANGE_LOADING_STATE, CHANGE_FLOWERS, LOAD_FLOWERS_REQUEST, LOAD_FLOWERS_SUCCESS, LOAD_FLOWERS_FAILURE } from "./types"

const initialStore = {
    data: [],
    error: null,
    isLoading: true
}

const reducer = (state = initialStore, action) => {
  switch(action.type) {
    case CHANGE_LOADING_STATE:
      return {...state, isLoading: action.payload}
    case CHANGE_FLOWERS:
      return {...state, data: action.payload}

    case LOAD_FLOWERS_REQUEST:
      return {...state, isLoading: true}
    case LOAD_FLOWERS_SUCCESS:
      return {...state, data: action.payload, isLoading: false}
    case LOAD_FLOWERS_FAILURE:
      return {...state, error: action.payload}
    default:
      return state
  }
}

export default reducer;