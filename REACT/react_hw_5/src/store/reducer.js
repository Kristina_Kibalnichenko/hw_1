import { combineReducers } from "redux";
import flowersReducer from './flowers/reducer'
import modalReducer from './modal/reducer'
import userReducer from './user/reducer'

const reducer = combineReducers({
    flowers: flowersReducer,
    modal: modalReducer,
    user: userReducer,
  })
export default reducer;