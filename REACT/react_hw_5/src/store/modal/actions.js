import { SET_MODAL_HEADER, CHANGE_MODAL_STATE, CHANGE_FLOWER, SET_MODAL_ADD_OR_DEL_VALUE } from "./types"

export const setModalHeader = (modalAddOrDeleteValue, itemAtCart) => {
    return {type: SET_MODAL_HEADER, payload: `Do you want to ${modalAddOrDeleteValue} ${itemAtCart} to or from cart?`}
}
export const changeModalState = (data) => ({type: CHANGE_MODAL_STATE, payload: data})
export const changeCurrentFlower = (flower) => ({type: CHANGE_FLOWER, payload: flower})
export const setModalAddOrDelValue = (data) => ({type: SET_MODAL_ADD_OR_DEL_VALUE, payload: data})
