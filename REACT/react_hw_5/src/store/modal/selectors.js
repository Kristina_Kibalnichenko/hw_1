export const getFlower = (state) => state.modal.flower
export const getModalState = (state) => state.modal.isActive
export const getCurrentModal = (state) => state.modal.currentModal
export const getModalAddOrDeleteValue = (state) => state.modal.modalAddOrDeleteValue