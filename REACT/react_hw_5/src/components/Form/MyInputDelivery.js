const MyInputDelivery = (props) => {
    const {label, classes, form, field} = props
    const {errors, touched} = form
    const {name} = field
    return (
        <div className={classes}>
            <label>
                {label}
                <textarea className="cart-input delivery" {...field} />
                {errors[name] && touched[name] && (
                    <span className="cart-errors">{errors[name]}</span>
                )}
                {!touched[name] && <span className="cart-errors"> </span>}
                {!errors[name] && touched[name] && <span className="cart-errors"> </span>}
            </label>
        </div>
    );
}

export default MyInputDelivery;