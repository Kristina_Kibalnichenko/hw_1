import React from 'react';
import './CartForm.scss'
import Button from "../Button/Button"
import { useDispatch, useSelector } from 'react-redux';
import { getFlowers } from "../../store/flowers/selectors"
import * as Yup from "yup"
import { Formik, Form, Field } from "formik"
import { setCartValues, deleteLocalStorageCart } from '../../store/user/actions'
import { changeFlowersList } from '../../store/flowers/actions'
import MyInput from './MyInput'
import MyInputDelivery from './MyInputDelivery'

const phoneRegExp = /^\+380\d{2}\d{3}\d{2}\d{2}$/
const CartForm = () => {
    const dispatch = useDispatch()
    const flowers = useSelector(getFlowers)
    const CheckoutSchema = Yup.object().shape({
        name: Yup.string()
                 .min(2, "Should contains at least 2 characters")
                 .max(30, "Should contains less than 30 characters")
                 .required("Required field"),
        surname: Yup.string()
                    .min(2, "Should contains at least 2 characters")
                    .max(30, "Should contains less than 30 characters")
                    .required("Required field"),
        age: Yup.number()
                .required("Required field")
                .positive("Should be a positive number)")
                .integer("Should be an integer number"),
        phone: Yup.string()
                  .matches(phoneRegExp, "Expexted format: +380XXXXXXXXX")
                  .required("Required field"), 
        delAddress: Yup.string()
                       .min(4, "Should contains at least 4 characters")
                       .max(200, "Should contains no more than 200 characters")
                       .required("Required field")
    })

    const submitForm = (values) => {
        const {name, surname, age, phone, delAddress} = values
        dispatch(setCartValues(values))
        
        const flowersAtCart = flowers.filter(el => el.amountAtCart !== 0)
        console.log('Purchased flowers:')
        flowersAtCart.forEach(({title, price, amountAtCart}, index) => {
            console.log(`${index + 1}. Flower name: ${title}, amount: ${amountAtCart}, full price: ${price * amountAtCart} UAH`)
        })
        console.log(`Info about user: \r\nname: ${name}, surname: ${surname}, age: ${age}, phone: ${phone}, delivery address: ${delAddress}`)
        
        const newFlowers = flowers.map(flower => {
            flower.amountAtCart = 0
            return flower
          })
        dispatch(changeFlowersList(newFlowers))
        dispatch(deleteLocalStorageCart())
    }

    return (
        <div>
            <h4>Please, fill in the fields below to make a purchase</h4>
            <Formik
                initialValues={{
                    name: '',
                    surname: '',
                    age: '',
                    phone: '',
                    delAddress: '',
                }}
                validationSchema={CheckoutSchema}
                onSubmit={submitForm}
            >
                {() => {
                    return (
                        <Form className="cart-fields-to-deliver">
                            <Field component={MyInput} type="text" name="name" label="Name:" classes="field name" placeholder=""/>
                            <Field component={MyInput} type="text" name="surname" label="Surname:" classes="field surname" placeholder=""/>
                            <Field component={MyInput} type="number" name="age" label="Age:" classes="field age" placeholder=""/>
                            <Field component={MyInput} type="tel" name="phone" label="Mobile phone:" classes="field phone" placeholder="+380501234567"/>
                            <Field component={MyInputDelivery} id="delAddress" name="delAddress" label="Delivery address:" classes="field deladr"/>
                            <div className="submit">
                                <Button
                                    backgroundColor="grey"
                                    text="Checkout"
                                    classBtn="cart-checkout"
                                    type="submit"
                                />
                            </div>
                        </Form>
                    )
                }}
            </Formik>
        </div>
    );
}

export default CartForm;