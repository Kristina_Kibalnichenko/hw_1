const MyInput = (props) => {
    const {type, label, classes, placeholder, form, field} = props
    const {errors, touched} = form
    const {name} = field
    return (
        <div className={classes}>
            <label>
                {label}
                <input type={type} className="cart-input" placeholder={placeholder} {...field} />
                {errors[name] && touched[name] && (
                    <span className="cart-errors">{errors[name]}</span>
                )}
                {!touched[name] && <span className="cart-errors"> </span>}
                {!errors.name && !errors.surname && !errors.age && !errors.phone && 
                (touched.name && touched.surname && touched.age && touched.phone && <span className="cart-errors"> </span>)}
            </label>
        </div>
    );
}

export default MyInput;