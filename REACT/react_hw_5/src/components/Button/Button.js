import React from 'react';
import PropTypes from 'prop-types';

const Button = ({backgroundColor, text, handleClick, classBtn, type="button"}) => {
    return (
      <button className={classBtn} style={{backgroundColor: backgroundColor}} onClick={handleClick} type={type}>
        {text}
      </button>
    );
}

export default Button;

Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string.isRequired,
  handleClick: PropTypes.func
}

Button.defaultProps = {
  classBtn: "button-class"
}