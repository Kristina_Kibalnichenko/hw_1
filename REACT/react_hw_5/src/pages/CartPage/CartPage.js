import ListOfFlowers from '../../components/ListOfFlowers/ListOfFlowers'
import CartForm from '../../components/Form/CartForm'
import './CartPage.scss';
import { useSelector } from 'react-redux';
import { getFlowers } from "../../store/flowers/selectors"

const CartPage = () => {
  const flowers = useSelector(getFlowers)
  const flowersAtCart = flowers.filter(el => el.amountAtCart !== 0)

  return (
    <div className="cart">
      {!!flowers.reduce((accumulator, currentValue) => accumulator + currentValue.amountAtCart, 0) &&
      <>
        <CartForm />
        <ListOfFlowers flowersAtPage={flowersAtCart} cart={true}/>
      </>}
    </div>
  );
}

export default CartPage;