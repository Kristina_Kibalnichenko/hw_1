import ListOfFlowers from '../../components/ListOfFlowers/ListOfFlowers'
import { useSelector } from 'react-redux';
import { getFlowers } from "../../store/flowers/selectors"

const FavoritesPage = () => {
  const flowers = useSelector(getFlowers)
  const flowersisFavorite = flowers.filter(el => el.isFavorite)

  return (
    <ListOfFlowers flowersAtPage={flowersisFavorite} />
  );
}

export default FavoritesPage;