import React, { useEffect } from 'react';
import './App.scss';
import Loading from './components/Loading/Loading';
import Header from './components/Header/Header'
import AppRoutes from './routes/AppRoutes'
import { useDispatch, useSelector } from 'react-redux';
import { getLoading } from "./store/flowers/selectors"
import { getItems } from "./store/flowers/operations"

const App = () => {
  const isLoading = useSelector(getLoading)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getItems())
  }, [dispatch]);

  if (isLoading) {
    return <Loading/>
  } 

  return (
    <div className="App">
      <Header />
      <AppRoutes />
    </div>
  );
}

export default App;
