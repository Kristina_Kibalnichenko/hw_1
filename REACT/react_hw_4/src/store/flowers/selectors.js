export const getLoading = (state) => state.flowers.isLoading
export const getFlowers = (state) => state.flowers.data