import { combineReducers } from "redux";
import flowersReducer from './flowers/reducer'
import modalReducer from './modal/reducer'

const reducer = combineReducers({
    flowers: flowersReducer,
    modal: modalReducer,
  })
export default reducer;