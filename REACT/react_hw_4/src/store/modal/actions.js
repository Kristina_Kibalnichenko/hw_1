import { SET_MODAL_HEADER, CHANGE_MODAL_STATE, CHANGE_FLOWER, SET_MODAL_ADD_OR_DEL_VALUE } from "./types"

// export const changeFlowesList = (data) => ({type: CHANGE_FLOWERS, payload: data})
// export const changeLoadingState = () => ({type: CHANGE_LOADING_STATE, payload: false})
export const setModalHeader = (modalAddOrDeleteValue, itemAtCart) => {
    return {type: SET_MODAL_HEADER, payload: `Do you want to ${modalAddOrDeleteValue} ${itemAtCart} to or from cart?`}
}
export const changeModalState = (data) => ({type: CHANGE_MODAL_STATE, payload: data})
export const changeCurrentFlower = (flower) => ({type: CHANGE_FLOWER, payload: flower})
export const setModalAddOrDelValue = (data) => ({type: SET_MODAL_ADD_OR_DEL_VALUE, payload: data})
// export const loadFlowersRequest = () => ({type: LOAD_FLOWERS_REQUEST})
// export const loadFlowersSuccess = (data) => ({type: LOAD_FLOWERS_SUCCESS, payload: data})
// export const loadFlowersFailure = (data) => ({type: LOAD_FLOWERS_FAILURE, payload: data})
