import { CHANGE_FLOWER, CHANGE_MODAL_STATE, SET_MODAL_ADD_OR_DEL_VALUE, SET_MODAL_HEADER } from "./types"

const initialStore = {
    isActive: false,
    flower: {},
    modalAddOrDeleteValue: "add",
    currentModal: {
      modalHeader: "",
      text: "Are you sure?",
      closeButton: true,
      buttonText: {
          okBtn: "Ok",
          cancelBtn: "Cancel"
      },
    }
}

const reducer = (state = initialStore, action) => {
  switch(action.type) {
    case CHANGE_FLOWER:
      return {...state, flower: action.payload}
    case CHANGE_MODAL_STATE:
      return {...state, isActive: action.payload}
    case SET_MODAL_ADD_OR_DEL_VALUE:
      return {...state, modalAddOrDeleteValue: action.payload}
    case SET_MODAL_HEADER:
      return {...state, currentModal: {...state.currentModal, modalHeader: action.payload}}
    default:
      return state
  }
}

export default reducer;