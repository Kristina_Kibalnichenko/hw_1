import React from 'react';
import './Modal.scss';
import Button from "../Button/Button";
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { getCurrentModal } from "../../store/modal/selectors"

const Modal = ({actions, closeModal}) => {
  const currentModal = useSelector(getCurrentModal)
    return (
      <div className="Modal-wrapper" onClick={() => closeModal()}>
        <div className="Modal" onClick={(e) => e.stopPropagation()}>
          <div className="Modal--header">
            <p className="Modal--header--text">{currentModal.modalHeader}</p>
            {currentModal.closeButton && <Button
                classBtn="closeBtn"
                backgroundColor="darkred"
                text=""
                handleClick={closeModal}
                />}
          </div>  
          <div className="Modal--body">
              {currentModal.text.split('. ').map((elem, index) => {
                  return (
                      <p key={index}>
                          {elem}
                          {index !== (currentModal.text.split('. ').length - 1) && "."}
                      </p>
                  )
              })}
          </div>
          <div className="Modal--btns-Ok-Cancel btns-Ok-Cancel">
              {actions.okButton()}
              {actions.cancelButton()}
          </div>
        </div>
      </div>
    );
}

export default Modal;

Modal.propTypes = {
  actions: PropTypes.object.isRequired,
  closeModal: PropTypes.func.isRequired
}