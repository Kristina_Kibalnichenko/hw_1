import React from 'react';
import './Flower.scss';
import Button from '../Button/Button'
import Icon from '../Icon/Icon'
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import { changeCurrentFlower, changeModalState, setModalAddOrDelValue } from "../../store/modal/actions"
import { changeFlowesList } from "../../store/flowers/actions"
import { getFlowers } from "../../store/flowers/selectors"

const Flower = ({flower}) => {
  const flowers = useSelector(getFlowers)
  const dispatch = useDispatch()

  const openModal = (addDelSwitcher=true) => {
    dispatch(changeCurrentFlower(flower))
    dispatch(changeModalState(true))
    dispatch(setModalAddOrDelValue(addDelSwitcher ? "add" : "delete"))
  }

  const addToFavorites = () => {
    const newFlowers = flowers.map(elem => {
      if (elem.vendorCode === flower.vendorCode) {
        elem.isFavorite = !elem.isFavorite
      }
      return elem
    })
    dispatch(changeFlowesList(newFlowers))

    let favor = JSON.parse(localStorage.getItem("favor")) || []
    favor = (favor.includes(flower.vendorCode) ? favor.filter(elem => elem !== flower.vendorCode) : favor.concat(flower.vendorCode))
    localStorage.setItem("favor", JSON.stringify(favor))
  }

  return (
    <li className="Flower-wrapper">
      <div className="Flower">
          <img src={flower.url} alt={flower.title} width='200' height='300'></img>
          <span>Flower title: <b>{flower.title}</b></span>
          <span>Vendor code: <i>{flower.vendorCode}</i></span>
          <span>Flower color: {flower.color}</span>
          <div className="Flower--favorites favorites">
            <span className="favorites--title">Add to favorites</span>
            <Icon
              onClick={addToFavorites}
              filled={flower.isFavorite}
              color="blue"
              className="favorites--icon"
            />
          </div>
          <div className="Flower--priceAndCartWrapper">
              <span className="Flower--price">{flower.price} UAH</span>
              <Button
                backgroundColor="#000"
                text="add to card"
                handleClick={openModal}
                classBtn="Flower--addToCardBtn"
              />
          </div>
      </div>
      <Button 
        backgroundColor="grey"
        text="X"
        handleClick={() => openModal(false)}
        classBtn="deleteFromCardBtn"
      />
    </li>
  );
}

export default Flower;

Flower.propTypes = {
  flower: PropTypes.exact({
    title: PropTypes.string,
    price: PropTypes.number,
    url: PropTypes.string,
    vendorCode: PropTypes.number,
    color: PropTypes.string,
    amountAtCart: PropTypes.number,
    isFavorite: PropTypes.bool
  }).isRequired
}