import React from 'react';

const Loading = () => {
    return (
      <div className="App" style={{textAlign: 'center', width: '100%', fontSize: '24px', fontWeight: 700}} >
        Loading....
      </div>
    );
}

export default Loading;