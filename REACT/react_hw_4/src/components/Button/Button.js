import React from 'react';
import PropTypes from 'prop-types';

const Button = ({backgroundColor, text, handleClick, classBtn}) => {
    return (
      <button className={classBtn} style={{backgroundColor: backgroundColor}} onClick={handleClick}>
        {text}
      </button>
    );
}

export default Button;

Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string.isRequired,
  handleClick: PropTypes.func.isRequired
}

Button.defaultProps = {
  classBtn: "button-class"
}