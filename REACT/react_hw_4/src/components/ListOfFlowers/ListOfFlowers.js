import React from 'react';
import Modal from '../Modal/Modal'
import Button from '../Button/Button'
import Flower from '../Flower/Flower';
import PropTypes from 'prop-types';
import './ListOfFlowers.scss';
import { useSelector, useDispatch } from 'react-redux';
import { changeModalState, setModalHeader } from "../../store/modal/actions"
import { changeFlowesList } from "../../store/flowers/actions"
import { getFlower, getModalState, getCurrentModal, getModalAddOrDeleteValue } from "../../store/modal/selectors"
import { getFlowers } from "../../store/flowers/selectors"
import { useEffect } from 'react';

const ListOfFlowers = ({flowersAtPage}) => {
  const dispatch = useDispatch()
  const flowers = useSelector(getFlowers)
  const flower = useSelector(getFlower)
  const isActive = useSelector(getModalState)
  const currentModal = useSelector(getCurrentModal)
  const modalAddOrDeleteValue = useSelector(getModalAddOrDeleteValue)
  
  const addToCart = () => {
    dispatch(changeModalState(false))
      
    const newFlowers = flowers.map(elem => {
      if (elem.vendorCode === flower.vendorCode) {
        elem.amountAtCart++
      }
      return elem
    })
    
    dispatch(changeFlowesList(newFlowers))
    let cart = JSON.parse(localStorage.getItem("cart")) || []
    cart = newFlowers.filter(elem => elem.amountAtCart !== 0)
    localStorage.setItem('cart', JSON.stringify(cart))
  }
    
  const deleteFromCart = () => {
    dispatch(changeModalState(false))
      
    const newFlowers = flowers.map(elem => {
      if (elem.vendorCode === flower.vendorCode) {
        elem.amountAtCart--
      }
      return elem
    })
    dispatch(changeFlowesList(newFlowers))

    let cart = JSON.parse(localStorage.getItem("cart")) || []
    cart = newFlowers.filter(elem => elem.amountAtCart !== 0)
    localStorage.setItem('cart', JSON.stringify(cart))
  }

  const closeModal = () => {
    dispatch(changeModalState(false))
  }

  useEffect(() => {
    // setModalHeader(`Do you want to ${modalAddOrDeleteValue} ${flower.title} to or from cart?`)
    dispatch(setModalHeader(modalAddOrDeleteValue, flower.title))
  }, [modalAddOrDeleteValue, flower.title, dispatch]);

  return (
    <>
      <ul className="listItemsWrapper">
        {flowersAtPage.map((el, index) => {
          return <Flower 
                    key={index} 
                    flower={el} 
                />
        })}
      </ul>
      {isActive && <Modal
        actions={{
          okButton: () => (<Button 
          text={currentModal.buttonText.okBtn}
          handleClick={(modalAddOrDeleteValue === "add") ? addToCart : deleteFromCart}
          classBtn="btns-Ok-Cancel--btn"/>),

          cancelButton: () => (<Button
            text={currentModal.buttonText.cancelBtn}
            handleClick={closeModal}
            classBtn="btns-Ok-Cancel--btn"/>)
          }}
        closeModal={closeModal}
      />}
    </>
  );
}

export default ListOfFlowers;

ListOfFlowers.propTypes = {
  flowersAtPage: PropTypes.arrayOf(PropTypes.object).isRequired,
}