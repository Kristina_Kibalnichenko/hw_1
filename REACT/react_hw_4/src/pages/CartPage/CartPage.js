import ListOfFlowers from '../../components/ListOfFlowers/ListOfFlowers'
import './CartPage.scss';
import { useSelector } from 'react-redux';
import { getFlowers } from "../../store/flowers/selectors"

const CartPage = () => {
  const flowers = useSelector(getFlowers)
  const flowersAtCart = flowers.filter(el => el.amountAtCart !== 0)

  return (
    <div className="cart">
      <ListOfFlowers flowersAtPage={flowersAtCart} />
    </div>
  );
}

export default CartPage;