import ListOfFlowers from '../../components/ListOfFlowers/ListOfFlowers'
import { useSelector } from 'react-redux';
import { getFlowers } from "../../store/flowers/selectors"

const Main = () => {
  const flowers = useSelector(getFlowers)
  return (
    <ListOfFlowers flowersAtPage={flowers} />
  );
}

export default Main;