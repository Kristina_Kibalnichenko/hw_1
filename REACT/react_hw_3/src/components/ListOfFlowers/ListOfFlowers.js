import React from 'react';
import Modal from '../Modal/Modal'
import Button from '../Button/Button'
import Flower from '../Flower/Flower';
import PropTypes from 'prop-types';
import './ListOfFlowers.scss';

const ListOfFlowers = (props) => {
  const {flowers, openModal, closeModal, addToCart, deleteFromCart, isActive, currentModal, modalHeader, addToFavorites, modalAddOrDeleteValue} = props;
  return (
    <>
      <ul className="listItemsWrapper">
        {flowers.map((el, index) => {
            return <Flower 
                      key={index} 
                      flower={el} 
                      openModal={openModal} 
                      addToFavorites={addToFavorites} 
                  />
        })}
      </ul>
      {isActive && <Modal
        header={modalHeader}
        text={currentModal.text}
        closeButton={currentModal.closeButton}
        actions={{
          okButton: () => (<Button 
          text={currentModal.buttonText.okBtn}
          handleClick={(modalAddOrDeleteValue === "add") ? addToCart : deleteFromCart}
          classBtn="btns-Ok-Cancel--btn"/>),

          cancelButton: () => (<Button
            text={currentModal.buttonText.cancelBtn}
            handleClick={closeModal}
            classBtn="btns-Ok-Cancel--btn"/>)
          }}
        isActive={isActive}
        closeModal={closeModal}
      />}
    </>
  );
}

export default ListOfFlowers;

ListOfFlowers.propTypes = {
  flowers: PropTypes.arrayOf(PropTypes.object).isRequired,
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  deleteFromCart: PropTypes.func.isRequired,
  addToCart: PropTypes.func.isRequired,
  isActive: PropTypes.bool.isRequired,
  currentModal: PropTypes.object.isRequired,
  modalHeader: PropTypes.string.isRequired,
  addToFavorites: PropTypes.func.isRequired,
  modalAddOrDeleteValue: PropTypes.string.isRequired
}