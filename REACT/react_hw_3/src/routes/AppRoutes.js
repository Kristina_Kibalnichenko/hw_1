import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Page404 from "../pages/Page404/Page404";
import Main from '../pages/Main/Main'
import CartPage from '../pages/CartPage/CartPage'
import FavoritesPage from '../pages/FavoritesPage/FavoritesPage'
import './AppRoutes.scss';

const AppRoutes = (props) => {
  return (
    <main className="app-routes">
      <Switch>
        <Redirect exact from="/" to="/home"/>
        <Route exact path="/home" render={() => <Main {...props}/>} />
        <Route exact path="/cart" render={() => <CartPage {...props}/>} />
        <Route exact path="/favorites" render={() => <FavoritesPage {...props}/>} />
        <Route path="*" component={Page404} />
      </Switch>
    </main>
  );
};

export default AppRoutes;
