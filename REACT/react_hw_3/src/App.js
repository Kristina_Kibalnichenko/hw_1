import React, { useEffect, useState } from 'react';
import './App.scss';
import axios from 'axios';
import Loading from './components/Loading/Loading';
import Header from './components/Header/Header'
import AppRoutes from './routes/AppRoutes'

const currentModal = {
  text: "Are you sure?",
  closeButton: true,
  buttonText: {
      okBtn: "Ok",
      cancelBtn: "Cancel"
  }
}

const App = () => {
  const [flowers, setFlowers] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const [isActive, setIsActive] = useState(false);
  const [flower, setFlower] = useState({});
  const [itemAtCart, setItemAtCart] = useState("");
  const [modalHeader, setModalHeader] = useState("");
  const [modalAddOrDeleteValue, setmodalAddOrDeleteValue] = useState("add");

  const normalizeData = (data) => {
    const favor = JSON.parse(localStorage.getItem('favor')) || []
    const cart = JSON.parse(localStorage.getItem('cart')) || []
    return data.map(elem => {
      elem.isFavorite = favor.includes(elem.vendorCode)
      let flag = true
      cart.forEach(item => {
        if (item.vendorCode === elem.vendorCode) {
          elem.amountAtCart = item.amountAtCart
          flag = false
        } 
      })
      if (flag) {
        elem.amountAtCart = 0
      }
      return elem
    })
  }

  useEffect(() => {
    axios('http://localhost:3000/itemslist.json')
      .then(result => {
        setFlowers(normalizeData(result.data.items))
        setLoading(false)
      })
  }, []);

  const openModal = (item, addDelSwitcher=true) => {
    setFlower(item)
    setIsActive(true)
    setItemAtCart(item.title)
    addDelSwitcher ? setmodalAddOrDeleteValue("add") : setmodalAddOrDeleteValue("delete")
  }

  useEffect(() => {
    setModalHeader(`Do you want to ${modalAddOrDeleteValue} ${itemAtCart} to or from cart?`)
  }, [itemAtCart, modalAddOrDeleteValue]);

  const closeModal = () => {
    setIsActive(false)
  }

  const addToCart = () => {
    setIsActive(false)
      
    const newFlowers = flowers.map(elem => {
      if (elem.vendorCode === flower.vendorCode) {
        elem.amountAtCart++
      }
      return elem
    })
    setFlowers(newFlowers)
    let cart = JSON.parse(localStorage.getItem("cart")) || []
    cart = newFlowers.filter(elem => elem.amountAtCart !== 0)
    localStorage.setItem('cart', JSON.stringify(cart))
  }

  const addToFavorites = (flower) => {
    const newFlowers = flowers.map(elem => {
      if (elem.vendorCode === flower.vendorCode) {
        elem.isFavorite = !elem.isFavorite
      }
      return elem
    })
    setFlowers(newFlowers)

    let favor = JSON.parse(localStorage.getItem("favor")) || []
    favor = (favor.includes(flower.vendorCode) ? favor.filter(elem => elem !== flower.vendorCode) : favor.concat(flower.vendorCode))
    localStorage.setItem("favor", JSON.stringify(favor))
  }
    
  const deleteFromCart = () => {
    setIsActive(false)
      
    const newFlowers = flowers.map(elem => {
      if (elem.vendorCode === flower.vendorCode) {
        elem.amountAtCart--
      }
      return elem
    })
    setFlowers(newFlowers)
    let cart = JSON.parse(localStorage.getItem("cart")) || []
    cart = newFlowers.filter(elem => elem.amountAtCart !== 0)
    localStorage.setItem('cart', JSON.stringify(cart))
  }

  if (isLoading) {
    return <Loading/>
  } 

  return (
    <div className="App">
      <Header flowers={flowers}/>
      <AppRoutes 
        flowers={flowers} 
        openModal={openModal} 
        closeModal={closeModal}
        addToCart={addToCart} 
        deleteFromCart={deleteFromCart}
        isActive={isActive} 
        currentModal={currentModal}
        modalHeader={modalHeader}
        addToFavorites={addToFavorites}
        modalAddOrDeleteValue={modalAddOrDeleteValue}
      />
    </div>
  );
}

export default App;
