import React from 'react';
import ListOfFlowers from '../../components/ListOfFlowers/ListOfFlowers'
import PropTypes from 'prop-types';

const FavoritesPage = (props) => {
  const {flowers, openModal, closeModal, addToCart, deleteFromCart, isActive, currentModal, modalHeader, addToFavorites, modalAddOrDeleteValue} = props;
  const flowersisFavorite = flowers.filter(el => el.isFavorite)

  return (
    <ListOfFlowers 
      flowers={flowersisFavorite} 
      openModal={openModal} 
      closeModal={closeModal}
      addToCart={addToCart}
      deleteFromCart={deleteFromCart}
      isActive={isActive}
      currentModal={currentModal}
      modalHeader={modalHeader} 
      addToFavorites={addToFavorites} 
      modalAddOrDeleteValue={modalAddOrDeleteValue} 
    />
  );
}

export default FavoritesPage;

FavoritesPage.propTypes = {
  flowers: PropTypes.arrayOf(PropTypes.object).isRequired,
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  deleteFromCart: PropTypes.func.isRequired,
  addToCart: PropTypes.func.isRequired,
  isActive: PropTypes.bool.isRequired,
  currentModal: PropTypes.object.isRequired,
  modalHeader: PropTypes.string.isRequired,
  addToFavorites: PropTypes.func.isRequired,
  modalAddOrDeleteValue: PropTypes.string.isRequired
  }