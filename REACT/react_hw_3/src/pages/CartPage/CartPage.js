import ListOfFlowers from '../../components/ListOfFlowers/ListOfFlowers'
import PropTypes from 'prop-types';
import './CartPage.scss';

const CartPage = (props) => {
  const {flowers, openModal, closeModal, addToCart, deleteFromCart, isActive, currentModal, modalHeader, addToFavorites, modalAddOrDeleteValue} = props;
  const flowersAtCart = flowers.filter(el => el.amountAtCart !== 0)

  return (
    <div className="cart">
      <ListOfFlowers 
          flowers={flowersAtCart} 
          openModal={openModal} 
          closeModal={closeModal}
          addToCart={addToCart}
          deleteFromCart={deleteFromCart}
          isActive={isActive}
          currentModal={currentModal}
          modalHeader={modalHeader} 
          addToFavorites={addToFavorites} 
          modalAddOrDeleteValue={modalAddOrDeleteValue}
      />
    </div>
  );
}

export default CartPage;

CartPage.propTypes = {
    flowers: PropTypes.arrayOf(PropTypes.object).isRequired,
    openModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
    deleteFromCart: PropTypes.func.isRequired,
    addToCart: PropTypes.func.isRequired,
    isActive: PropTypes.bool.isRequired,
    currentModal: PropTypes.object.isRequired,
    modalHeader: PropTypes.string.isRequired,
    addToFavorites: PropTypes.func.isRequired,
    modalAddOrDeleteValue: PropTypes.string.isRequired
  }