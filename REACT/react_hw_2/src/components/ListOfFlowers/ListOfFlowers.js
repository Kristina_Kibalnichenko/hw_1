import React, { Component } from 'react';
import Flower from '../Flower/Flower'
import PropTypes from 'prop-types';

class ListOfFlowers extends Component {
  render() {
    const {flowers, openModal, addToFavorites} = this.props
    
    return (
          <ul className="listItemsWrapper">
            {flowers.map((el, index) => {
                return <Flower key={index} flower={el} openModal={openModal} addToFavorites={addToFavorites} />
            })}
        </ul>
    );
  }
}

export default ListOfFlowers;

ListOfFlowers.propTypes = {
  flowers: PropTypes.arrayOf(PropTypes.object).isRequired,
  openModal: PropTypes.func.isRequired,
  addToFavorites: PropTypes.func.isRequired
}