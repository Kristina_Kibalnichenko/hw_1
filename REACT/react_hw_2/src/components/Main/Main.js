import React, { Component } from 'react';
import './Main.scss';
import ListOfFlowers from '../ListOfFlowers/ListOfFlowers'
import Modal from '../Modal/Modal'
import Button from '../Button/Button'
import PropTypes from 'prop-types';

class Main extends Component {
  render() {
    const {flowers, openModal, closeModal, addToCart, isActive, currentModal, addToFavorites} = this.props
    
    return (
      <main className="Main">
        <ListOfFlowers flowers={flowers} openModal={openModal} addToFavorites={addToFavorites} />
        {isActive && <Modal
          header={currentModal.header}
          text={currentModal.text}
          closeButton={currentModal.closeButton}
          actions={{
            okButton: () => (<Button 
            text={currentModal.buttonText.okBtn}
            handleClick={addToCart}
            classBtn="btns-Ok-Cancel--btn"/>),

            cancelButton: () => (<Button
              text={currentModal.buttonText.cancelBtn}
              handleClick={closeModal}
              classBtn="btns-Ok-Cancel--btn"/>)
            }}
          isActive={isActive}
          closeModal={closeModal}
        />}
      </main>
    );
  }
}

export default Main;

Main.propTypes = {
  flowers: PropTypes.arrayOf(PropTypes.object).isRequired,
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  addToCart: PropTypes.func.isRequired,
  isActive: PropTypes.bool.isRequired,
  currentModal: PropTypes.object.isRequired,
  addToFavorites: PropTypes.func.isRequired
}