import React, { Component } from 'react';
import './Flower.scss';
import Button from '../Button/Button'
import Icon from '../Icon/Icon'
import PropTypes from 'prop-types';

class Flower extends Component {
  render() {
    const {flower, openModal, addToFavorites} = this.props
    return (
      <li>
        <div className="Flower">
            <img src={flower.url} alt={flower.title} width='200' height='300'></img>
            <span>Flower title: <b>{flower.title}</b></span>
            <span>Vendor code: <i>{flower.vendorCode}</i></span>
            <span>Flower color: {flower.color}</span>
            <div className="Flower--favorites favorites">
              <span className="favorites--title">Add to favorites</span>
              <Icon
                onClick={() => addToFavorites(flower)}
                filled={flower.isFavorite}
                color="blue"
                className="favorites--icon"
              />
            </div>
            <div className="Flower--priceAndCartWrapper">
                <span className="Flower--price">{flower.price} UAH</span>
                <Button
                    backgroundColor="#000"
                    text="add to card"
                    handleClick={() => openModal(flower)}
                    classBtn="Flower--addToCardBtn"
                />
            </div>
        </div>
      </li>
    );
  }
}

export default Flower;

Flower.propTypes = {
  flower: PropTypes.exact({
    title: PropTypes.string,
    price: PropTypes.number,
    url: PropTypes.string,
    vendorCode: PropTypes.number,
    color: PropTypes.string,
    amountAtCart: PropTypes.number,
    isFavorite: PropTypes.bool
  }).isRequired,
  openModal: PropTypes.func.isRequired,
  addToFavorites: PropTypes.func.isRequired
}