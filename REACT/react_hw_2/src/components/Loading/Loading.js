import React, { Component } from 'react';

class Loading extends Component {
  render() {
    return (
      <div style={{textAlign: 'center', width: '100%', fontSize: '24px', fontWeight: 700}} >
        Loading....
      </div>
    );
  }
}

export default Loading;