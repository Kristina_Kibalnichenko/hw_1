import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Button extends Component {
  render() {
    const {backgroundColor, text, handleClick, classBtn} = this.props
    return (
      <div className="button">
        <button className={classBtn} style={{backgroundColor: backgroundColor}} 
                        onClick={handleClick}>{text}</button>
      </div>
    );
  }
}

export default Button;

Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string.isRequired,
  handleClick: PropTypes.func.isRequired
}

Button.defaultProps = {
  classBtn: "button-class"
}