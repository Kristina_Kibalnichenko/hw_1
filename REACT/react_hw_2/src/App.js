import React, { Component } from 'react';
import './App.scss';
import axios from 'axios';
import Loading from './components/Loading/Loading';
import Header from './components/Header/Header'
import Main from './components/Main/Main'

const currentModal = {
  header: "Do you want to add this item to cart?",
  // header: "",
  text: "Are you sure?",
  closeButton: true,
  buttonText: {
      okBtn: "Ok",
      cancelBtn: "Cancel"
  }
}

class App extends Component {
  state = {
    flowers: [],
    isLoading: true,
    isActive: false,
    flower: {},
  }

  componentDidMount() {
    axios('http://localhost:3000/itemslist.json')
      .then(result => {
        this.updateFlowers(this.normalizeData(result.data.items))
        this.updateLoading(false)
      })
  }

  normalizeData = (data) => {
    const favor = JSON.parse(localStorage.getItem('favor')) || []
    const cart = JSON.parse(localStorage.getItem('cart')) || []
    return data.map(elem => {
      elem.isFavorite = favor.includes(elem.vendorCode)
      let flag = true
      cart.forEach(item => {
        if (item.vendorCode === elem.vendorCode) {
          elem.amountAtCart = item.amountAtCart
          flag = false
        } 
      })
      if (flag) {
        elem.amountAtCart = 0
      }
      return elem
    })
  }

  updateFlowers = (data) => {
    this.setState({flowers: data})
  }
 
  updateLoading = (data) => {
    this.setState(() => ({isLoading: data})) 
  }

  openModal = (item) => {
    this.setState({ flower : item })
    // this.setState(() => ({ header : `Do you want to add ${this.state.flower.title} to cart?`}))
    this.setState(() => ({ isActive : true }))
  }

  closeModal = () => {
    this.setState({ isActive : false})
  }

  addToCart = () => {
    const {flower, flowers} = this.state
    this.setState({ isActive : false})
      
    const newFlowers = flowers.map(elem => {
      if (elem.vendorCode === flower.vendorCode) {
        elem.amountAtCart++
      }
      return elem
    })
    this.setState({flowers: newFlowers})
    let cart = JSON.parse(localStorage.getItem("cart")) || []
    cart = newFlowers.filter(elem => elem.amountAtCart !== 0)
    localStorage.setItem('cart', JSON.stringify(cart))

    // const temp = JSON.parse(localStorage.getItem("cart"))
    // console.log("cart at localStorage: ", temp)
  }

  addToFavorites = (flower) => {
    const {flowers} = this.state
    const newFlowers = flowers.map(elem => {
      if (elem.vendorCode === flower.vendorCode) {
        elem.isFavorite = !elem.isFavorite
      }
      return elem
    })
    this.setState({flowers: newFlowers})

    let favor = JSON.parse(localStorage.getItem("favor")) || []
    favor = (favor.includes(flower.vendorCode) ? favor.filter(elem => elem !== flower.vendorCode) : favor.concat(flower.vendorCode))
    localStorage.setItem("favor", JSON.stringify(favor))
    
    // const temp = JSON.parse(localStorage.getItem("favor"))
    // console.log("favorites at localStorage: ", temp)
  }

  render() {
    const {flowers, isLoading, isActive} = this.state
    
    if (isLoading) {
      return <Loading/>
    } 

    return (
      <div className="App">
        <Header flowers={flowers}/>
        <Main flowers={flowers} 
              openModal={this.openModal} 
              closeModal={this.closeModal}
              addToCart={this.addToCart} 
              isActive={isActive} 
              currentModal={currentModal}
              addToFavorites={this.addToFavorites}
        />
      </div>
    );
  }
}

export default App;
