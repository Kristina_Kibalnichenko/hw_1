"use strict";

const URI = "https://swapi.dev/api/films/";

function getMovies() {
    return fetch(URI)
    .then(response => response.json());
}

function getCharacters(peremURI) {
    return fetch(peremURI)
    .then(response => response.json());
}

async function uploadCharacters(characters, ul) {
    let arr = [];
    let arrPromises = [];
    characters.forEach((elem) => {
        const li = document.createElement("li");
        arrPromises.push(getCharacters(elem).then(({name}) => {
            li.textContent = name;
        }));
        arr.push(li);
    });

    await Promise.all([...arrPromises]);
    
    const li = document.createElement('li');
    li.textContent = "characters : ";
    ul.append(li);
    
    const ulCharacter = document.createElement('ul');
    li.append(ulCharacter);
    ulCharacter.append(...arr);
    return false;
}

getMovies()
    .then(({results}) => {
        results.forEach(({title, episode_id: episodeNumber, opening_crawl: shortIntro, characters}) => {
            const ul = document.createElement('ul');
            document.body.append(ul);
            let moviesItems = {episodeNumber, shortIntro, title};

            for (let key in moviesItems) {
                const li = document.createElement('li');
                li.textContent = key + " : " + moviesItems[key];
                ul.append(li);
            }
            uploadCharacters(characters, ul);
        });
    }) 
    .catch((error) => {
        console.log(error.message);
    });