"use strict";

function createNewUser() {
    let firstName, lastName, birthday, dayBirthday, monthBirthday, yearBirthday;
    const now = new Date;

    do {
        firstName = prompt("Enter your first name:");
    } while (firstName === null || firstName === "" || firstName.length < 2);

    do {
        lastName = prompt("Enter your last name:");
    } while (lastName === null || lastName === "" || lastName.length < 2);

    do {
        birthday = prompt("Enter your date of birth (use the following format \"dd.mm.yyyy\"):");
        dayBirthday = +birthday.slice(0, 2);
        monthBirthday = +birthday.slice(3, 5);
        yearBirthday = +birthday.slice(6);
    } while (birthday === null || birthday === "" || birthday.trim().length !== 10 || 
             isNaN (+birthday.slice(0, 1)) || isNaN (+birthday.slice(1, 2)) || isNaN (+birthday.slice(3, 4)) || 
             isNaN (+birthday.slice(4, 5)) || isNaN (+birthday.slice(6, 7)) || isNaN (+birthday.slice(7, 8)) || 
             isNaN (+birthday.slice(8, 9)) || isNaN (+birthday.slice(9)) || 
             birthday.slice(2, 3) !== "." || birthday.slice(5, 6) !== "." ||
             dayBirthday > 31 || monthBirthday > 12 || yearBirthday < (now.getFullYear()-150) ||
             (new Date(yearBirthday, monthBirthday-1, dayBirthday)) > now);
    
    const newUser = {
        firstName,
        lastName,
        birthday : new Date(yearBirthday, monthBirthday-1, dayBirthday),
        getLogin() {
            return this.firstName.trim().slice(0, 1).toLocaleLowerCase() +this.lastName.trim().toLocaleLowerCase();
        },
        getAge() {
            return Math.floor((now - this.birthday) / (1000 * 60 * 60 * 24 * 365));
        },
        getPassword() {
            return this.firstName.trim().slice(0, 1).toUpperCase() + this.lastName.trim().toLocaleLowerCase() + this.birthday.getFullYear();
        },
    };
    
    // console.log("Your login: " + newUser.getLogin());
    console.log("You are " + newUser.getAge() + " years old");
    console.log("Your password: " + newUser.getPassword());
    
    return newUser;
};

console.log(createNewUser());