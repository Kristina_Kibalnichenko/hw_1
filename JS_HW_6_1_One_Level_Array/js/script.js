"use strict";

function filterBy(arr, typeOfArray) {
    let result;
    arr.forEach((value) => {
        result = arr.filter((value) => typeof value !== typeOfArray);
    });
    return result;
};

console.log(filterBy(["arr", false, function(){}, null, 234, 
                      {name : "Vasya", num : [23, 14, "5"], addObject : {id : [12, 24], password : 12345}, color : "black"},
                      ["name", "surname", 25], "end"], 'object'));