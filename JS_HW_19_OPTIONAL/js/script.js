"use strict";

function estimateDeadline(teamSpeed, backlog, deadline) {
    let teamSpeedPerDay = teamSpeed.reduce((summ, currValue) => summ + currValue, 0);
    let backlogStoryPoints = backlog.reduce((summ, currValue) => summ + currValue, 0);

    const now = new Date;
    const temp = new Date;
    
    let generalDays = Math.ceil(((new Date(deadline)) - now.setDate(now.getDate() - 1)) / (1000 * 60 * 60 * 24));

    let flag = true;
    let multiply, finalCount;
    let count = 0;
    let daysAmmount = 0;
    while (flag) {
        daysAmmount++;
        let day = temp.getDay();
        
        if (day === 0 || day === 6) {
            multiply = 0;
        } else {
            multiply = 1;
        };

        count += teamSpeedPerDay * multiply;        
        if (count >= backlogStoryPoints) {
            flag = false;
        };
        
        temp.setDate(temp.getDate() + 1);

        if (generalDays === daysAmmount) {
            finalCount = count;
        };
    };

    let result = generalDays - daysAmmount;

    if (result >= 0) {
        alert(`Все задачи будут успешно выполнены за ${result} дней до наступления дедлайна!`);
        return false;
    } else {
        let hoursLack = backlogStoryPoints - finalCount;
        alert(`Команде разработчиков придется потратить дополнительно ${hoursLack} часов после дедлайна, 
               чтобы выполнить все задачи в беклоге`);
        return false;
    };
};

console.log(estimateDeadline([20, 15, 10], [4, 2, 200, 58,12], "2020-11-02"));