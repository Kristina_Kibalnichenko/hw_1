"use strict";

//TASK 1
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
let newClientsArr = new Set([...clients1, ...clients2]);
console.log("#1: newClientsArr: ", newClientsArr);


//TASK 2
const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17, 
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vempire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

let charactersShortInfo = [];
characters.forEach(function(item, index) {
    let {name, lastName, age} = characters[index];
    charactersShortInfo[index] = {name, lastName, age};
});
console.log("#2: charactersShortInfo: ", charactersShortInfo);


//TASK 3
// const user1 = {
//     name: "John",
//     years: 30
// };

// let {name: name, years: age, isAdmin: isAdmin = false} = user1;
// console.log("#3: variables: ", name, age, isAdmin);


//TASK 4
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
        lat: 38.869422, 
        lng: 139.876632
    }
}

const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
}

const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
}

let fullProfile = {...satoshi2018, ...satoshi2019, ...satoshi2020};
console.log("#4: fullProfile: ", fullProfile);


//TASK 5
// const books = [{
//     name: 'Harry Potter',
//     author: 'J.K. Rowling'
// }, {
//     name: 'Lord of the rings',
//     author: 'J.R.R. Tolkien'
// }, {
//     name: 'The witcher',
//     author: 'Andrzej Sapkowski'
// }];

// const bookToAdd = {
//     name: 'Game of thrones',
//     author: 'George R. R. Martin'
// }

// let newBooksArr = [...books];
// let {name, author} = bookToAdd;
// newBooksArr.push({name, author});
// console.log("#5: initial array: ", books);
// console.log("#5: newBooksArr: ", newBooksArr);


//TASK 6
const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

let {name, surname, age = 40, salary = "unknown"} = employee;
let newEmployee = {
    name,
    surname,
    age, 
    salary,
};
console.log("#6: initial object: ", employee);
console.log("#6: new object: ", newEmployee);


//TASK 7
const array = ['value', () => 'showValue'];
let [value, showValue] = array;

alert(value); // должно быть выведено 'value'
alert(showValue());  // должно быть выведено 'showValue'
