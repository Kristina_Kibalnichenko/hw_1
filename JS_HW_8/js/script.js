"use strict";

const label = document.createElement("label");
label.setAttribute("for", "price");
label.textContent = "Price: ";
label.style.cssText = "margin: 15px";
document.body.append(label);

const input = document.createElement("input");
input.setAttribute("type", "text");
input.setAttribute("id", "price");
label.append(input);

let flagDiv = true, flagSpan = true, value = 0;
input.addEventListener("focus", function setInputGreenBorder(event) {
    if (!flagDiv) {
        const div = document.getElementsByClassName("created")[0];
        div.remove();
        flagDiv = true;
    };

    if (!flagSpan) {
        const span = document.getElementsByClassName("spanstyle")[0];
        const btnX = document.getElementById("btnstyle");
        const br = document.getElementById("brstyle");
        span.remove();
        btnX.remove();
        br.remove();
        flagSpan = true;
    };

    event.target.style.color = "black"; 
    event.target.style.fontWeight = "normal";
    event.target.style.border = "5px solid rgba(0, 177, 106, 0.7)";
});

input.addEventListener("blur", function deleteInputGreenBorder(event) {
    value = event.target.value;
    event.target.style.border = "1px solid black";

    if (value === "" || isNaN(+value)) {
        if (flagDiv) {
            let div = document.createElement("div");
            div.textContent = "Value must includes ONLY NUMBERS";
            div.classList.add("created");
            input.after(div);
            flagDiv = false;
        };
    } else if (value < 0) {
        let div = document.createElement("div");
        div.textContent = "Please enter correct price (Value must contains only Positive numbers)";
        div.classList.add("created");
        input.after(div);
        flagDiv = false;
        event.target.style.border = "5px solid red";
    } else {
        event.target.style.color = "green"; 
        event.target.style.fontWeight = "bold";
        
        const span = document.createElement("span");
        span.classList.add("spanstyle");
        span.style.border = "none";
        span.textContent = "";
        span.textContent = `Текущая цена: ${value}  `;

        const btnX = document.createElement("button");
        btnX.id = "btnstyle";
        btnX.textContent = "x";
        btnX.style.borderRadius = "50%";
        btnX.style.border = "1px solid grey";
        btnX.style.background = "lightslategrey";
        
        const br = document.createElement("br");
        br.id = "brstyle";

        document.body.prepend(span, btnX, br);

        flagSpan = false;
            
        btnX.addEventListener("click", function deleteSpan(event) {
            btnX.remove();
            span.remove();
            br.remove();
            input.value = 0;
            flagSpan = true;
      });
    };
});