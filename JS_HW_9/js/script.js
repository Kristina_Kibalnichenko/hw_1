"use strict";

let tab = function () {
    let tabs = document.querySelectorAll('.tabs-title'),
        tabsContent = document.querySelectorAll('.tab'),
        tabName;
    
        tabs.forEach(elem => {
            elem.addEventListener("click", selectTab);
        });

        function selectTab() {
            tabs.forEach(elem => {
                elem.classList.remove('active');
            });
            this.classList.add('active');
            tabName = this.getAttribute('data-tab-name');
            selectTabContent(tabName);
        }

        function selectTabContent(tabName) {
            tabsContent.forEach(elem => {
                if (elem.classList.contains(tabName)) { 
                    elem.classList.add('active')
                } else {
                    elem.classList.remove('active');
                };
            });
        }
};

tab();