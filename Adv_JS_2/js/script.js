"use strict";

const books = [
    { 
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70 
    }, 
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    }, 
    { 
        name: "Тысячекратная мысль",
        price: 70
    }, 
    { 
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    }, 
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];  

const div = document.getElementById("root");
let ul, li;

books.forEach(function(item, index) {
    let {author, name, price} = item;
    try {
        if (author != null && name != null && price != null) {
            ul = document.createElement("ul");
            div.append(ul);
            
            for (let perem in item) {
                li = document.createElement("li");
                if (typeof(item[perem]) !== 'number') {
                    li.textContent = `${perem} : "${item[perem]}"`;
                } else {
                    li.textContent = `${perem} : ${item[perem]}`;
                };
                ul.append(li);
            };
        } else {
            if (author == null) {
                throw new Error(`property author is absent at ${index + 1} object`);
            };
            if (name == null) {
                throw new Error(`property name is absent at ${index + 1} object`);
            };
            if (price == null) {
                throw new Error(`property price is absent at ${index + 1} object`);
            };
        };
    } catch (error) {
        console.log(error.message);
    };
});
