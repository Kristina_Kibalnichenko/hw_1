"use strict";

function makeListFromArray(arr, parent = "document.body") {
    if (arr.length !== 0) {
        let ul = document.createElement("ul");

        const tegArr = parent.split(".");
        let fullTeg = "";
        for (let i = 1; i < tegArr.length; i++) {
            fullTeg += tegArr[i];
            if (i !== (tegArr.length -1)) {
                fullTeg += " > ";
            };
        };
        let finalTeg = document.querySelector(fullTeg);
        finalTeg.append(ul);

        const arrItems = arr.map((element) => {
            if (!Array.isArray(element)) {
                let li = document.createElement("li");
                li.textContent = `Город - ${element}`;
                ul.append(li);
                return li;
            } else {
                parent += ".ul";
                return makeListFromArray(element, parent);
            };
        }); 

        setTimeout(() => ul.remove(), 3000);
    };
    return;
};

makeListFromArray(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", 
                   ["Lviv", "Dnieper", ["Vyshneve", "Vinnitsya"], "Bucha"]], "document.body.p.span");

