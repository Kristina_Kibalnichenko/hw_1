"use strict";

// localStorage.removeItem("oldTheme");
// localStorage.removeItem("currentTheme");
const button = document.getElementById('change-color-theme');
const footer = document.getElementsByClassName('footer-container')[0];
footer.before(button);

let newTheme = '', serialOldColorTheme = '', currentTheme = '';

function getColors() {
    const arrElemColor = [document.getElementsByClassName("logo")[0],
                        document.getElementsByClassName("logochangecolor")[0],
                        document.getElementsByClassName("header__menu")[0],
                        document.getElementsByClassName("aside__menu__item")[0],
                        document.getElementsByClassName("footer__nav")[0]
    ];

    const arrElemBgColor = [document.getElementsByClassName("aside-container")[0], 
                            document.getElementsByClassName("img-container")[0], 
                            document.getElementsByClassName("footer-container")[0]
    ];

    const oldColorTheme = {};
    let i = 0,
        theCSSprop;
    arrElemColor.forEach(elem => {  
        theCSSprop = window.getComputedStyle(elem,null).getPropertyValue("color");
        oldColorTheme[i] = theCSSprop;
        i++;
    });

    arrElemBgColor.forEach(elem => {  
        theCSSprop = window.getComputedStyle(elem,null).getPropertyValue("background-color");
        oldColorTheme[i] = theCSSprop;
        i++;
    });
    serialOldColorTheme = JSON.stringify(oldColorTheme); 
};
getColors();

function reWriteColors() {
    document.getElementsByClassName("logo")[0].style.color = newTheme[0];
    document.getElementsByClassName("logochangecolor")[0].style.color = newTheme[1];
    document.getElementsByClassName("header__menu")[0].style.color = newTheme[2];
    document.getElementsByClassName("aside__menu__item")[0].style.color = newTheme[3];
    document.getElementsByClassName("footer__nav")[0].style.color = newTheme[4];
    document.getElementsByClassName("aside-container")[0].style.backgroundColor = newTheme[5];
    document.getElementsByClassName("img-container")[0].style.backgroundColor = newTheme[6];
    document.getElementsByClassName("footer-container")[0].style.backgroundColor = newTheme[7];
};

if (localStorage.getItem("oldTheme") != null && localStorage.getItem("currentTheme") !== serialOldColorTheme) {
    currentTheme = localStorage.getItem("currentTheme");
    newTheme = JSON.parse(currentTheme);
    localStorage.setItem("oldTheme", serialOldColorTheme);
    reWriteColors();
};

button.addEventListener("click", function switchColorTheme() {
    getColors();
    if (localStorage.getItem("oldTheme") == null) {
        newTheme = {0: 'rgb(255,0,0)', 1: 'rgb(0,0,0)', 2: 'rgb(220,20,60)', 3: 'rgb(0,100,0)', 4: 'rgb(105,105,105)', 
                              5: 'rgb(224,255,255)', 6: 'rgb(224,255,255)', 7: 'rgba(47,79,79,0.5)'};
        currentTheme = JSON.stringify(newTheme);
        localStorage.setItem("currentTheme", currentTheme);
        localStorage.setItem("oldTheme", serialOldColorTheme);
    } else {
        currentTheme = localStorage.getItem("oldTheme");
        newTheme = JSON.parse(currentTheme);
        const temtTheme = localStorage.getItem("currentTheme");
        localStorage.setItem("oldTheme", temtTheme); 
        localStorage.setItem("currentTheme", currentTheme); 
    };
    reWriteColors();
});