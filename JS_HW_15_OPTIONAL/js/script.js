"use strict";

let number, newNumber;

do {
    number = prompt("Enter a natural number:", number);
} while (number === null || number === "" || isNaN(+number) || +number !== Math.floor(+number)) {
    newNumber = +number;
    console.log(`Факториал числа ${newNumber} равен:`);
}

function factorial(x) {
    if (x < 0) {
        console.log("Ошибка! Факториал может быть вычислен только для неотрицательного числа");
        return;
    } else if (x === 1 || x === 0) {
      return 1;
    } else {
      return x * factorial(x - 1);
    }
}

console.log(factorial(newNumber));



