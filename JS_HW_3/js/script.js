"use strict";

let num1, num2, operand;

do {
    num1 = prompt("Enter first number:", num1);
} while (num1 === null || num1 === "" || isNaN(+num1)) 

do {
    num2 = prompt("Enter second number:", num2);
} while (num2 === null || num2 === "" || isNaN(+num2))

do {
    operand = prompt("Enter a sign of Math operation (\"+\"  or  \"-\"  or  \"*\"  or  \"/\"):", operand);
} while (operand !== "+" && operand !== "-" && operand !== "*" && operand !== "/")

const simpleMathOperation = function (x, y, operation) {
    if (operation === "+") {
        return +x + +y;
    } else if (operation === "-") {
        return x - y;
    } else if (operation === "*") {
        return x * y;
    } else if (operation === "/") {
        return x / y;
    }
}
console.log(simpleMathOperation(num1, num2, operand));



