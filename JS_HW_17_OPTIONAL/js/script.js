"use strict";

    const student = {
        name : "", 
        lastName : "",
    };

    let firstName, surname;
    do {
        firstName = prompt("Enter your first name");
    } while (firstName === null || firstName === "" || firstName.length < 2);

    do {
        surname = prompt("Enter your last name");
    } while (surname === null || surname === "" || surname.length < 2);

    student.name = firstName;
    student.lastName = surname;
    // console.log(student);

    let score, subject;
    student.tabel = {};
    while (subject !== null) {
        do {
            subject = prompt("Enter a Subject:");
            // console.log(`Subject: ${subject}`);
        } while (subject === "");
        if (subject !== null) {
            do {
                score = prompt("Enter a score of this Subject (valid range from \"1\" to \"12\"):");
            } while (score === "" || score === null || isNaN(+score) || +score !== Math.floor(+score) || score < 1 || score > 12);
            student.tabel[subject] = score;
        };
    };
    // console.log(student.tabel);
    
    let badScoreNumber = 0;
    for (let key in student.tabel) {
        if (student.tabel[key] < 4) {
            badScoreNumber++;
        }
    };
    if (badScoreNumber < 1) {
        alert("Студент переведен на следующий курс");
    } else {
        alert(`Количество плохих (меньше 4) оценок по предметам = ${badScoreNumber}`);
    };

    let averageScore = 0;
    let index = 0;
    for (let key in student.tabel) {
        averageScore += +student.tabel[key];
        index++;
    };
    averageScore /= index;
    if (averageScore > 7) {
        alert("Студенту назначена стипендия");
    } 
    