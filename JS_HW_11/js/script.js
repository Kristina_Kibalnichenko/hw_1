"use strict";

let changeKeyColor = function () {
    const keys = document.querySelectorAll('.btn');
    
    document.addEventListener("keydown", function(event) {
        keys.forEach(elem => {
            let keyLetter = event.key.slice(0, 1).toUpperCase() + event.key.slice(1);
            if (elem.textContent === keyLetter) {                
                changeToBlackColor();
                elem.style.backgroundColor = "#0000ff";
            };
        });
        
        function changeToBlackColor() {
            keys.forEach(elem => {
                elem.style.backgroundColor = "#000000";
            });
        };
        
    });
};

changeKeyColor();