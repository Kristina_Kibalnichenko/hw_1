"use strict";

const result = [];
function filterBy(arr, typeOfArray) {
    for (const key in arr) {
        const value = arr[key];

        if (typeof value === 'object') {
            if (typeOfArray !== 'object') {
                if (value !== null) {
                    filterBy(value, typeOfArray);
                } else {
                    result.push(value);
                };
            };
        } else {
            if (typeof value !== typeOfArray) {
                result.push(value);
            };
        }
    }
    
    return result;
    
}

console.log(filterBy(["arr", false, function(){}, null, 234, 
                      {name : "Vasya", num : [23, 14, "5"], addObject : {id : [12, 24], password : 12345}, color : "black"},
                      ["name", "surname", 25], "end"], 'object'));